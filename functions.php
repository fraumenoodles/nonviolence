<?php
// Remove all default WP template redirects/lookups
remove_action('template_redirect', 'redirect_canonical');

// Redirect all requests to index.php so the Vue app is loaded and 404s aren't thrown
function remove_redirects() {
    add_rewrite_rule('^/(.+)/?', 'index.php', 'top');
}
add_action('init', 'remove_redirects');

add_theme_support( 'post-thumbnails' ); 

// Load scripts
function load_vue_scripts() {

	wp_enqueue_style('mapbox.css','https://api.tiles.mapbox.com/mapbox.js/v3.1.1/mapbox.css', false, null);
	wp_enqueue_style('mapbox-gl.css','https://api.tiles.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.css', false, null);
	wp_enqueue_script('mapbox.js','https://api.tiles.mapbox.com/mapbox.js/v3.1.1/mapbox.js', null, null, true);
	wp_enqueue_style('fullpage.css', 'https://unpkg.com/fullpage.js/dist/fullpage.min.css',false,null);
	wp_enqueue_script('mapbox-gl.js', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.js', null, null, true);
	wp_enqueue_script('element.js', "//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit", null, null, true);
    wp_enqueue_script('blankslate/manifest.js', get_template_directory_uri() . '/dist/scripts/manifest.js', null, null, true);
    wp_enqueue_script('blankslate/manifest.js', get_template_directory_uri() . '/dist/scripts/manifest.js', null, null, true);
    wp_enqueue_style('blankslate/app.css', get_template_directory_uri() . '/dist/styles/app.css', false, null);
    wp_enqueue_script('blankslate/manifest.js', get_template_directory_uri() . '/dist/scripts/manifest.js', null, null, true);
    wp_enqueue_script('blankslate/vendor.js', get_template_directory_uri() . '/dist/scripts/vendor.js', null, null, true);
    wp_enqueue_script('blankslate/app.js', get_template_directory_uri() . '/dist/scripts/app.js', null, null, true);
}
add_action('wp_enqueue_scripts', 'load_vue_scripts', 100);

function my_acf_google_map_api( $api ){
	
	$api['key'] = 'AIzaSyAZGwNRYYAhTjLOocHTNhQkpxLcqfsQ32Y';
	return $api;
	
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');


//add endpoint
function add_options(){
	$post_type_query  = new WP_Query(  
    	array (  
        	'post_type'      => 'post',  
        	'posts_per_page' => -1  
    	)  
	);

	$posts_array      = $post_type_query->posts;

	$story_index = array();

	foreach ($posts_array as $post){
		$story = array("ID" => $post->ID, "map" => get_post_meta($post->ID, "map"));
		array_push($story_index, $story);
	}	


	update_option( 'stories_bundle', $story_index);
}

add_role(
    'application_user',
    __( 'Application User' ),
    array(
        'edit_posts'   => true,
    )
);



function remove_from_index($post){
	$stories = get_option('stories_bundle');
	foreach($stories as $story){
		if($story["ID"] == $post->ID){
			array_diff($stories, $story);
			break;
		}
	}
}

function add_to_index ($ID, $post){
	$stories = get_option('stories_bundle');
	foreach($stories as $story){
		if($story["ID"] == $ID){
			//array_diff($stories, $story);
			break;
		}
	}
	$story = array("ID" => $ID, "map" => get_post_meta($ID, "map"));
	array_push($stories, $story);

}


add_action( 'publish_post', 'add_to_index', 10, 2 );
add_action( 'publish_to_pending', 'remove_from_index', 10, 1);
add_action( 'publish_to_private', 'remove_from_index', 10, 1);
add_action( 'publish_to_draft', 'remove_from_index', 10, 1);
add_action( 'publish_to_trash', 'remove_from_index', 10, 1);
add_action( 'publish_to_future', 'remove_from_index', 10, 1);

function add_stories_endpoint(){
	register_rest_route( 'wp/v2', '/posts/story-index', array(
    'methods' => 'GET',
    'callback' => 'get_story_index',
  ));
}

add_action( 'rest_api_init', 'add_stories_endpoint');

add_action('init', 'add_options');

function get_story_index() {
	$stories = get_option('stories_bundle');
	return $stories;
}

$args = array(
    'type' => 'string',
    'single' => true,
	'show_in_rest' => true,
);

register_meta( 'post', 'author_email', $args );

//add_action( 'new_to_draft', 'dispatch_notifications', 100, 1);
add_action( 'rest_insert_post', 'dispatch_notifications', 10, 3);

function dispatch_notifications($post, $request, $false){

	$author = get_user_by( 'id', $post->post_author);

	if( $author->user_login == "app_user"){

		$request_body = json_decode($request->get_body());
		send_thank_you($request_body->meta->author_email);

		$args = array(
		    'role__in' => array('Administrator', 'Editor'),
		);
		
		$users = get_users( $args );

		foreach($users as $user){
			alert_editor($user->user_email);
		}
	}
}

function send_thank_you($email){
	wp_mail($email, "Thank you for your submission.", "Thank you for your submission to Nonviolence Now. Your story of nonviolence is greatly appreciated. We will notify you when your story appears on our map. \n\n The Nonviolence Now Team");
}

function alert_editor($email){
	wp_mail($email, "New story in queue", "There is a new story to review in the story queue.");
}


//add post published notification

function send_story_notification($email, $url){

}


// Function to change email address
function wpb_sender_email( $original_email_address ) {
    return 'noreply@nonviolencenow.org';
}
 
// Function to change sender name
function wpb_sender_name( $original_email_from ) {
    return 'Nonviolence Now';
}
 
// Hooking up our functions to WordPress filters
add_filter( 'wp_mail_from', 'wpb_sender_email' );
add_filter( 'wp_mail_from_name', 'wpb_sender_name' );
add_filter('acf/settings/remove_wp_meta_box', '__return_false');
// Enable the option show in rest
add_filter( 'acf/rest_api/field_settings/show_in_rest', '__return_true' );

// Enable the option edit in rest
add_filter( 'acf/rest_api/field_settings/edit_in_rest', '__return_true' );

