import api from '../../api'
import * as types from '../mutation-types'

// initial state
const state = {
  error: null,
  notice: null,
  loading: true,
  loading_progress: 0,
  first_visit: true
}

// getters
const getters = {
  isLoading: state => state.loading_progress < 100,
  loadingProgress: state=> state.loading_progress,
  loadingIncrement: state => { return 100 / SETTINGS.LOADING_SEGMENTS },
  firstVisit: state => state.first_visit,
}

// actions
const actions = {
	visited (context) {
		context.commit('visited')
	}
}

// mutations
const mutations = {
  [types.INCREMENT_LOADING_PROGRESS] (state, val) {
    state.loading_progress = Math.min(state.loading_progress + getters.loadingIncrement(), 100)
  },

  [types.RESET_LOADING_PROGRESS] (state) {
    state.loading_progress = 0
  },

  visited (state){
		state.first_visit = false;
  }

}

export default {
  state,
  getters,
  actions,
  mutations
}
