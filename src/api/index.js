export default {

  getCategories (cb) {
    axios.get(window.SETTINGS.API_BASE_PATH + 'categories?sort=name&hide_empty=true&per_page=50')
      .then(response => {
        cb(response.data.filter(c => c.name !== "Uncategorized"))
      })
      .catch(e => {
        cb(e)
      })
  },

  submitStory(submission) {
	axios.post(window.SETTINGS.API_BASE_PATH + 'posts', {
		title: "Draft Submission",
		meta:{
				author_email: submission.email,
		},
		fields:{
			location: submission.location,
			date: submission.date,
		},
		content: submission.description,
	}, 
	{
    	headers: {'Authorization' : 'Basic ' + btoa('app_user'+':'+'nonviolence-now')}
	}).then(response => { console.log(response)})
	  .catch(e => {
    	console.log(e.response);
	})
  },

  getPages (cb) {
    axios.get(window.SETTINGS.API_BASE_PATH + 'pages?per_page=10')
      .then(response => {
        cb(response.data)
      })
      .catch(e => {
        cb(e)
      })
  },

  getPage (id, cb) {
    if (_.isNull(id) || !_.isNumber(id)) return false
    axios.get(window.SETTINGS.API_BASE_PATH + 'pages/'+id)
      .then(response => {
        cb(response.data)
      })
      .catch(e => {
        cb(e)
      })
  },

  getPosts (limit, cb) {
    if (_.isEmpty(limit)) { let limit = 5 }
    
    axios.get(window.SETTINGS.API_BASE_PATH + 'posts?per_page='+limit)
      .then(response => {
        cb(response.data)
      })
      .catch(e => {
        cb(e)
      })
  },
  
  getPostsCategory (limit, category_id, cb){
    if (_.isEmpty(limit)) { let limit = 5 }
    
    axios.get(window.SETTINGS.API_BASE_PATH + 'posts?per_page='+limit+'?categories='+category_id)
      .then(response => {
        cb(response.data)
      })
      .catch(e => {
        cb(e)
      })
  },

} 
