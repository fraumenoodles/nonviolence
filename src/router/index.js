import Vue from 'vue'
import Router from 'vue-router'

// Components
import Home from '../components/Home.vue'
import Post from '../components/Post/Post.vue'
import Page from '../components/Page/Page.vue'
import Share from '../components/Page/Share.vue'
import Story from '../components/Story/Story.vue'
import Category from '../components/Category/Category.vue'
import Principles from '../components/Category/Principles.vue'
import Map from '../components/Map.vue'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      children: [{
      	path: '/stories/:postSlug',
      	name: 'Post',
      	component: Story
	  }],
	},
	{
	  path: '/demo',
	  name: 'Map',
	  component: Map
	},
	{
	  path: '/share-your-story',
	  name: 'Share',
	  component: Share
    },
	{
	  path: '/principles',
	  name: 'Principles',
	  component: Principles,
	},
    {
      path: '/:pageSlug',
      name: 'Page',
      component: Page
    },
	{
      path: '/principles/:categorySlug',
      name: 'Category',
      component: Category
    }
  ],
  mode: 'history',
  base: '',

  // Prevents window from scrolling back to top
  // when navigating between components/views
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else if ((from.name == 'Home' || from.name == 'Post') && to.name == 'Post' ) {
		return false
    }else return { x: 0, y: 0 }
  }
})

export default router
